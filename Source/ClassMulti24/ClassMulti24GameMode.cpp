// Copyright Epic Games, Inc. All Rights Reserved.

#include "ClassMulti24GameMode.h"
#include "ClassMulti24Character.h"
#include "UObject/ConstructorHelpers.h"

AClassMulti24GameMode::AClassMulti24GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
