// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClassMulti24GameMode.generated.h"

UCLASS(minimalapi)
class AClassMulti24GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AClassMulti24GameMode();
};



